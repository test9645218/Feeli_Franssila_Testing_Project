
## Summary (Summarize the bug encountered concisely)

    On the "Create new project" (https://gitlab.com/projects/new) page there is a typo and instead of "Create blank project" it reads as "Create black project".

## Steps to reproduce     
    This bug is encountered by going to the (https://gitlab.com/projects/new) page through any methods.
   

## What is the current bug behavior?
    Current bug behavuor is that in the https://gitlab.com/projects/new page there is a wrong title ("Create black project") instead of ("Create blank project").   

## What is the expected correct behavior?
    Correct behavior is that the title should be "Create blank project" instead of "Create black project".

     
## Relevant logs and/or screenshots
    https://gyazo.com/6c246293a28107a7cf3e648c2aef8f7a

## Possible fixes
    Change the name to "Create blank project".


## Whom do you report/ Assign To/ Tags
    /label ~bug ~reproduced ~needs-investigation 
    /cc @project-manager 
    /assign @qa-tester


## Priority
    Trivial
      
